%{
#include <iostream>
#include <string>
#include "s1041412.h"
using namespace std;
extern int yylex();
string gate;
void yyerror(char *);
%}
%union 
{ 
    int number;
}
%token <number> NUMBER 
%type <number> lines expression term
%nonassoc ASSIGN COMMA NLINE 
%nonassoc BUFF NOT AND NAND OR NOR XOR NXOR 
%left INPUT OUTPUT
%left COMMENT
%%
lines   : expression { $$ = $1; } 
        | lines expression NLINE { }
        | lines term NLINE { }
        | lines COMMENT NLINE { }
        ;
expression  : term { $$ = $1; }
            | INPUT '(' NUMBER ')' 
            { 
                gatedata[gatenumber[0][0]+3][gatenumber[1][0]] = $3;
                //cout << "INPUT(" << gatedata[gatenumber[0][0]+3][gatenumber[1][0]] <<")\n";
                gatenumber[1][0]++;
            }
            | OUTPUT '(' NUMBER ')' 
            { 
                gatedata[gatenumber[0][9]+3][gatenumber[1][9]] = $3;
                //cout << "OUTPUT("<<gatedata[gatenumber[0][9]+3][gatenumber[1][9]] <<")\n";
                gatenumber[1][9]++;
            }
            ;
term    : 
        | NUMBER ASSIGN BUFF '(' NUMBER ')'
        { 
            gatedata[gatenumber[0][1]+3][gatenumber[1][1]] = $1;
            gatedata[gatenumber[0][1]+4][gatenumber[1][1]] = $5;
            //cout<< gatedata[gatenumber[0][1]+3][gatenumber[1][1]] << "=BUFF(" <<  gatedata[gatenumber[0][1]+4][gatenumber[1][1]] << ")\n";
            gatenumber[1][1]++;
        }
        | NUMBER ASSIGN NOT '(' NUMBER ')'
        {
            gatedata[gatenumber[0][2]+3][gatenumber[1][2]] = $1;
            gatedata[gatenumber[0][2]+4][gatenumber[1][2]] = $5;
            //cout<< gatedata[gatenumber[0][2]+3][gatenumber[1][2]] << "=NOT(" <<  gatedata[gatenumber[0][2]+4][gatenumber[1][2]] << ")\n";
            gatenumber[1][2]++;
        }
        | NUMBER ASSIGN AND '(' NUMBER COMMA NUMBER ')'
        {
            gatedata[gatenumber[0][3]+3][gatenumber[1][3]] = $1;
            gatedata[gatenumber[0][3]+4][gatenumber[1][3]] = $5;
            gatedata[gatenumber[0][3]+5][gatenumber[1][3]] = $7;
            //cout<< gatedata[gatenumber[0][3]+3][gatenumber[1][3]] << "=AND(" <<  gatedata[gatenumber[0][3]+4][gatenumber[1][3]] << "," <<  gatedata[gatenumber[0][3]+5][gatenumber[1][3]] << ")\n";
            gatenumber[1][3]++;
        }
        | NUMBER ASSIGN NAND '(' NUMBER COMMA NUMBER ')'
        {
            gatedata[gatenumber[0][4]+3][gatenumber[1][4]] = $1;
            gatedata[gatenumber[0][4]+4][gatenumber[1][4]] = $5;
            gatedata[gatenumber[0][4]+5][gatenumber[1][4]] = $7;
            //cout<< gatedata[gatenumber[0][4]+3][gatenumber[1][4]] << "=NAND(" <<  gatedata[gatenumber[0][4]+4][gatenumber[1][4]] << ","  <<  gatedata[gatenumber[0][4]+5][gatenumber[1][4]] << ")\n";
            gatenumber[1][4]++;
        }
        | NUMBER ASSIGN OR '(' NUMBER COMMA NUMBER ')' 
        {
            gatedata[gatenumber[0][5]+3][gatenumber[1][5]] = $1;
            gatedata[gatenumber[0][5]+4][gatenumber[1][5]] = $5;
            gatedata[gatenumber[0][5]+5][gatenumber[1][5]] = $7;
            //cout<< gatedata[gatenumber[0][5]+3][gatenumber[1][5]] << "=OR(" <<  gatedata[gatenumber[0][5]+4][gatenumber[1][5]] << ","  <<  gatedata[gatenumber[0][5]+5][gatenumber[1][5]] << ")\n";
            gatenumber[1][5]++;
        }
        | NUMBER ASSIGN NOR '(' NUMBER COMMA NUMBER ')' 
        {
            gatedata[gatenumber[0][6]+3][gatenumber[1][6]] = $1;
            gatedata[gatenumber[0][6]+4][gatenumber[1][6]] = $5;
            gatedata[gatenumber[0][6]+5][gatenumber[1][6]] = $7;
            //cout<< gatedata[gatenumber[0][6]+3][gatenumber[1][6]] << "=NOR" <<  gatedata[gatenumber[0][6]+4][gatenumber[1][6]] << ","  <<  gatedata[gatenumber[0][6]+5][gatenumber[1][6]] << ")\n";
            gatenumber[1][6]++;
        }
        | NUMBER ASSIGN XOR '(' NUMBER COMMA NUMBER ')' 
        {
            gatedata[gatenumber[0][7]+3][gatenumber[1][7]] = $1;
            gatedata[gatenumber[0][7]+4][gatenumber[1][7]] = $5;
            gatedata[gatenumber[0][7]+5][gatenumber[1][7]] = $7;
            //cout<< gatedata[gatenumber[0][7]+3][gatenumber[1][7]] << "=XOR(" <<  gatedata[gatenumber[0][7]+4][gatenumber[1][7]] << ","  <<  gatedata[gatenumber[0][7]+5][gatenumber[1][7]] << ")\n";
            gatenumber[1][7]++;
        }
        | NUMBER ASSIGN NXOR '(' NUMBER COMMA NUMBER ')' 
        {
            gatedata[gatenumber[0][8]+3][gatenumber[1][8]] = $1;
            gatedata[gatenumber[0][8]+4][gatenumber[1][8]] = $5;
            gatedata[gatenumber[0][8]+5][gatenumber[1][8]] = $7;
            //cout<< gatedata[gatenumber[0][8]+3][gatenumber[1][8]] << "=NXOR(" <<  gatedata[gatenumber[0][8]+4][gatenumber[1][8]] << ","  <<  gatedata[gatenumber[0][8]+5][gatenumber[1][8]] << ")\n";
            gatenumber[1][8]++;
        }
        ;
%%
void yyerror(char *s) 
{    
   cout<< "a" << s;
}